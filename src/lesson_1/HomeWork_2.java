package lesson_1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by shtativ on 19.09.15.
 */

/*
>дано линейное уравнение:
 C
1.создать две переменные
2.инициализировать
3.вычислить x
4.вывести на экран:
"х = результат"

 */

public class HomeWork_2 {
    public static void main(String[] args) throws IOException {
        //создание и инициализация переменных c клавиатуры
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        double a;
        double b;
        System.out.println("Input a:");
        a = Double.parseDouble(reader.readLine());
        System.out.println("Input b:");
        b = Double.parseDouble(reader.readLine());
        double x = -b / a;

        System.out.println("x = " + x);
    }
}



