package lesson_2;

/**
 * Created by shtativ on 22.09.15.
 */
public class Lesson_2{
    public static void main(String[] args) {
        int intVar = 111;
        double douleVar = 3.14;

//        int a = 12;
//        int b = 20;
//        double x = (double) -b / a; //явное преобразование типов

        String strVar = "String variable";
        char charVar = '\u090F';
        char ch = 'h';

        boolean bool = true;
        bool = 4 != 6;

        System.out.println(bool);
        System.out.println((char) intVar); //преобразование Int в char
        System.out.println(strVar + " " + intVar + " " + charVar);

        if (bool) {
            System.out.println("bool = true");
        } else {
            System.out.println("bool = false");
        }



        int a = 4;
        int b = 4;
        int c = 4;

       if (a != b && a != b && b != c) {
            if (a < b && a < c ) {
                System.out.println("Минимальное число = " + a);
            } else if (b < c && b < a) {
                System.out.println("Минимальное число = " + b);
            } else
                System.out.println("Минимальное число = " + c);

        }

        String str1 = "name";
        String str2 = "Name";
        
// цикл по строк
        if(str1.charAt(0) < str2.charAt(0)){
            System.out.println(str1);
        }
        else
            System.out.println(str2);
    }
}
