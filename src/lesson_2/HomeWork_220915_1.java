package lesson_2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by shtativ on 24.09.15.
 */
public class HomeWork_220915_1 {
    public static void main(String[] args) throws IOException {
        //Ввод коэффицетов переменных с клавиатуры
        //*при желании заменить 'int' на 'double'
        BufferedReader scanInt = new BufferedReader(new InputStreamReader(System.in));

        System.out.println("Variable a = ");
        int a = Integer.parseInt(scanInt.readLine());
        System.out.println("Variable b = ");
        int b = Integer.parseInt(scanInt.readLine());
        System.out.println("Variable c = ");
        int c = Integer.parseInt(scanInt.readLine());

            if (a <= b && a <= c ) {
                System.out.println("Min = " + a);
            } else
            if (b <= a && b <= c) {
                System.out.println("Min = " + b);
            } else
                System.out.println("Min = " + c);
    }
}
