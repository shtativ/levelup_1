package lesson_2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by shtativ on 25.09.15.
 */
public class HomeWork_220915_2 {
    public static void main(String[] args) throws IOException {
        BufferedReader readVar = new BufferedReader(new InputStreamReader(System.in));
        int a, b, c;
        double D;
        double x1, x2;
        System.out.println("Для решения квадратного уравнения вида ax^2+bx+c=0");
        System.out.println("Введите коэффицент переменной 'a':");
        a = Integer.parseInt(readVar.readLine());
        System.out.println("Введите коэффицент переменной 'b':");
        b = Integer.parseInt(readVar.readLine());
        System.out.println("Введите коэффицент переменной 'c':");
        c = Integer.parseInt(readVar.readLine());

        //вычисление дискриминанта D
        D = (Math.pow(b, 2)) - (4 * a * c);
        System.out.println("D = " +D);
        //вычссление значенияS переменной x, в зависимости от полученного дискриминанта
        if (D > 0) {
            x1 = (-b + Math.sqrt(D)) / (2 * a);
            x2 = (-b - Math.sqrt(D)) / (2 * a);
            System.out.println("Так как D > 0, то уравнение имеет два корня: \n x1 = " + x1 + "\n x2 = " + x2);
        }
        else {
            if (D == 0){
                x1 = (-b + Math.sqrt(D)) / (2 * a);
                x2 = x1;
                System.out.println("Так как D = 0, то уравнение имеет два равных корня: /\nx1 = " + x1 + "\nx2 = " + x2);
            }
            else
                //D < 0
                System.out.println("Так как D < 0, то корни уравнения не вещественные");
            //добавить комплексные числа

        }
    }
}
