package lesson_7;

import com.sun.org.apache.xalan.internal.xsltc.runtime.*;

import java.util.*;
import java.util.Hashtable;

/**
 * Created by shtativ on 09.10.15.
 */


public class ArrayListLesson {
    public static void main(String[] args) {
        ArrayList<String> strList = new ArrayList<String>();

        //классы обертки
        ArrayList<Integer> intList = new ArrayList<Integer>();
        /*
        class Integer {
        int value;
        }
         */
        Integer x = 6;
        Integer y = 5;
        int[] array = new int[10];
        intList.add(5);

        //HashTable/HashMap
        //Все, что оканчивается на Map и на Set - не упорядоченно
        Hashtable<String, String> table = new Hashtable<String, String>();
        table.put("Vova", "1234");
        table.put("Vova", "12345");
        table.get("Vova");
        String phone = table.get("Vova");//get получает значение по ключу
        //если нет ключа, то phone == null
        table.containsKey("Vova"); //true, если есть ключ
        String dima = table.put("Dima", null); //некорректная запись
        if (table.containsKey("Vova")) {
            String pho = table.get("Dima");
            System.out.println(pho);
        }

        //Set - мнодество
        HashSet<Integer> intSet = new HashSet<Integer>();
        intSet.add(5);
        intSet.add(5);


        HashSet<Object> objSet = new HashSet<Object>();
        Object o = new Object();
        objSet.add(o);
        //o.setName() например
        objSet.add(o); //происходит перезапись объекта

        System.out.println(objSet.size());

        HashMap<Integer, String> tab = new HashMap<Integer, String>();
        for (int i = 0; i < 10; i++) {
            tab.put(i, "text"+i);
        }

        Iterator<Integer> it = tab.keySet().iterator();
        for(int key : tab.keySet() ){
            System.out.println(key);
        }

    }
}

/*
==================================ДЗ==================================
1) классы-обертки
2) параметризация класса
3) HaspTable и HashMap
4) http://habrahabr.ru/post/162017/
5) ВСЕ ПОЛЯ private, изменить на public по необходимости (get, set) ИНКАПСУЛЯЦИЯ
    public - доступ из любого класса
    default - ?
    protected - почти как private, применяется в наследовании
6) static ?
7) final - капсом VALUE
 */

//1) отличия public от default
//2) Сэджвик "Алгоритмы на JAVA"  тема "Связанные списки"
//    реализовать односвязный список с добавлением и удалением переменной
//    хранить контакты
// БЕЗ МАССИВОВ
//3) Инкапсуляция
//4) подготовиться к наследованию/полиморфизму   и Swing
//5)