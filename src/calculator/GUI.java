//package calculator;
//
//import calculator.listener.ArithmeticButtonListener;
//import calculator.listener.NumButtonListener;
//import calculator.listener.ResultButtonListener;
//
//import javax.swing.*;
//import java.awt.*;
//import java.awt.event.ActionEvent;
//import java.awt.event.ActionListener;
//
///**
// * Created by shtativ on 20.10.15.
// */
//public class GUI {
//    public void build(){
//        JFrame mainFrame = new JFrame("Calculator");
//        mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
//        mainFrame.setBounds(100, 100, 500, 300);
//        mainFrame.setVisible(true);
//
//        JLabel display = new JLabel("0");
//
//        // меню
//        //JMenuBar - верхняя панель(без самих пунктов)
//        //JMenu - сами пункты (Файл, Правка,...)
//        //      - подменю
//        //JMenuItem - пункт конкретного меню(Открыть, Сохранить,....)
//        //JCheckBoxMenuItem - галочка выбора
//
//        JMenuBar bar = new JMenuBar();
//        JMenu fileMenu = new JMenu("File");
//        JMenuItem quitMenuItem = new JMenuItem("Quit");
//        fileMenu.add(quitMenuItem);
//        bar.add(fileMenu);
//        quitMenuItem.addActionListener(new ActionListener() {
//            @Override
//            public void actionPerformed(ActionEvent e) {
//                System.exit(0);
//            }
//        });
//
//        JMenu viewMenu = new JMenu("View");
//        JMenuItem menuItem1 = new JMenuItem("menu item 1");
//        JMenuItem menuItem2 = new JMenuItem("menu item 2");
//        JMenuItem menuItem3 = new JMenuItem("menu item 3");
//        viewMenu.add(menuItem1);
//        viewMenu.add(menuItem2);
//        viewMenu.add(menuItem3);
//        bar.add(viewMenu);
//        menuItem1.addActionListener(new ActionListener() {
//            @Override
//            public void actionPerformed(ActionEvent e) {
//
//            }
//        });
// menuItem2.addActionListener(new ActionListener() {
//            @Override
//            public void actionPerformed(ActionEvent e) {
//
//            }
//        });
// menuItem3.addActionListener(new ActionListener() {
//            @Override
//            public void actionPerformed(ActionEvent e) {
//
//            }
//        });
//
//
//        JPanel numPanel = new JPanel(new GridLayout(4, 3));
//        JButton[] numB = new JButton[10];
////        NumButtonListener numBListener = new NumButtonListener(display);
////        for (int i = 0; i < numB.length; i++) {
////            numB[i] = new JButton(String.valueOf(i));
////            numB[i].addActionListener(numBListener);
////        }
//        JButton unMinusB = new JButton("-/+");
//        JButton dotB = new JButton(".");
//        for (int i = 7; i < 10; i++) {
//            numPanel.add(numB[i]);
//        }
//        for (int i = 4; i < 7; i++) {
//            numPanel.add(numB[i]);
//        }
//        for (int i = 1; i < 4; i++) {
//            numPanel.add(numB[i]);
//        }
//
//        numPanel.add(unMinusB);
//        numPanel.add(numB[0]);
//        numPanel.add(dotB);
//
//
//        //Арифметические операции
//
//        JPanel rightPanel = new JPanel(new GridLayout(2, 1));
//        JPanel arifmPanel = new JPanel(new GridLayout(2, 2));
//        JButton plusB = new JButton("+");
//        JButton minusB = new JButton("-");
//        JButton nullB = new JButton("0");
//        JButton divB = new JButton("/");
//        JButton resultB = new JButton("=");
//
//        ResultButtonListener resultListener = new ResultButtonListener();
//        resultB.addActionListener(resultListener);
//        ArithmeticButtonListener arithmeticButtonListener = new ArithmeticButtonListener();
//
//        arifmPanel.add(plusB);
//        arifmPanel.add(minusB);
//        arifmPanel.add(nullB);
//        arifmPanel.add(resultB);
//        rightPanel.add(arifmPanel);
//        rightPanel.add(resultB);
//        mainFrame.add(rightPanel, BorderLayout.EAST);
//
//        mainFrame.add(display, BorderLayout.NORTH);
//
//    }
//}
