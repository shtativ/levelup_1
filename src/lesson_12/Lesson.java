package lesson_12;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.LinkedList;

/**
 * Created by shtativ on 27.10.15.
 */
public class Lesson {
    public static void main(String[] args) {
       // int[] array = new int[5];
        //array[5] = 0;
        //RruntimeException - не подчеркиваются в коде
       /* int a = 6;
        int x = 5/6;

        //Exceptions
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        try {
            reader.readLine();
        } catch (IOException e){

            e.printStackTrace();
        }
        //Alt+Enter -автоматическая подстановка rty-catch
        try {
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }


        //try - catch на обе строки
        try {
            reader.readLine();
            //дальнейший код выполнится при условии, если верхняя строка не выбросит Exception
            reader.close();
        } catch (IOException e){
            e.printStackTrace();
        }*/


        //ОДНОСВЯЗНЫЕ СПИСКИ
        ArrayList<String> arrayList = new ArrayList<String>();
        LinkedList<String> linkedList = new LinkedList<String>();

        for (int i = 0; i < 1000000 ; i++) {
            arrayList.add("line " + i);
        }
        System.out.println("ArrayList done");
        for (int i = 0; i < 1000000; i++) {
            linkedList.add("line " + i);
        }
        System.out.println("LinkedList done");
    }
}


/**   ============ДЗ 27.10.15===============
 * 1) Регулярные выражения regex.com
 * 2) try/catch
 * 3) разобраться с односвязными списками
 * 4) Преобразовать в двусвязный циклический список
 * 5) Сэджвик "Алгоритмы на Jav"
 * 6) Роберт Лафоре "Алгоритмы и структуры "
 */