package lesson_11;

import java.io.*;
import java.util.ArrayList;
//java.io - Input/Output

/**
 * Created by shtativ on 23.10.15.
 */
public class Main {
    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        Integer.parseInt(reader.readLine());
        reader.close(); //завершение потока!

        BufferedReader reader1 = new BufferedReader(new InputStreamReader(new FileInputStream("file.txt")));

//выводит на консоль все строчки из файла, проверяя наличие следующей строки

        //ЗАПИСЬ В ФАЙЛ
        //true - запись в конец
        PrintWriter writer = new PrintWriter(new FileOutputStream("outputFile.txt", true));

        writer.println("line1 - 1");
        writer.println("line1 - 2");

        //всегда использовать
        writer.flush(); //запись данных из кэша/буфера в файл

        writer.close();
    }
}
/*
считываеие всегда с первой строки, без индексации
 */

//     23.10.15
/* Телефонная книга:
1) Сохранение контактов перед выходом из программы в менюбаре
2) Считывание контактов при запуске программы

Крестики-нолики:
1) Сохранение состояния игрового поля в файл
2) При запуске программы подгружать сохраненное игровое поле
3) добавить в меню бар пункты:  ОСТАЛЬНОЕ НА GitHub
Теория:
1) переменные окружения
 */