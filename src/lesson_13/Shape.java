package lesson_13;

/**
 * Created by shtativ on 06.11.15.
 */
public abstract class Shape {
    protected double S;
    protected int angleCount;

    public double getS(){
        return S;
    }

    public int getAngleCount() {
        return angleCount;
    }

    public void show() {
        System.out.println("Shape");
    }

    public abstract void getMassCenter(){
    }
}
