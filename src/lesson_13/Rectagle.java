package lesson_13;

/**
 * Created by shtativ on 06.11.15.
 */
public class Rectagle extends Shape implements AngleInterface {
    protected double width;
    protected double height;

//    public Rectagle() {
//        super();
//    }

    public Rectagle(double width, double height) {
        this.width = width;
        this.height = height;
        this.S = width * height;
        this.angleCount = 4;
    }

    //переопределение метода
    @Override
    public void show()      {
        super.show();
        System.out.println("-->Rectangle");

    }

    public double getWidth() {
        return width;
    }
}
