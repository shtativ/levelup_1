package lesson_8.listeners;


import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
/**
 * Created by shtativ on 15.10.15.
 */
public class DecFontSizeListener implements ActionListener {
    private JTextArea textAreaDec;

    public DecFontSizeListener (JTextArea textArea){
        this.textAreaDec = textArea;
    }

    public void actionPerformed(ActionEvent e){
        int newSize = textAreaDec.getTabSize()-1;
        this.textAreaDec.setFont(new Font("Arial", Font.BOLD, textAreaDec.getFont().getSize()-1));
    }

}
