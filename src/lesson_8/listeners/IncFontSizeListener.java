package lesson_8.listeners;



import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by shtativ on 13.10.15.
 */
public class IncFontSizeListener implements ActionListener {
    private JTextArea textAreaInc;

    public IncFontSizeListener(JTextArea textArea ) {
        this.textAreaInc = textArea;
    }

    public void actionPerformed(ActionEvent e) {
        int newSize = textAreaInc.getTabSize()+1;
        this.textAreaInc.setFont(new Font("Arial", Font.BOLD, textAreaInc.getFont().getSize()+1));
    }

}
