package lesson_8.listeners;

import lesson_8.Lesson_8;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by shtativ on 13.10.15.
 */
public class ClearButtonListener implements ActionListener {
   private JTextArea clr;
    public ClearButtonListener(JTextArea clr ) {
        this.clr = clr;
    }

    //очистка поля ввода
    public void actionPerformed(ActionEvent e) {
        clr.setText("");
    }
}
