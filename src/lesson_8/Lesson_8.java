package lesson_8;

import lesson_8.listeners.ClearButtonListener;
import lesson_8.listeners.DecFontSizeListener;
import lesson_8.listeners.IncFontSizeListener;
import java.awt.Color;

import javax.swing.*;
import java.awt.*;

/**
 * Created by shtativ on 13.10.15.
 */

public class Lesson_8 {
    public static void main(String[] args) {
        JFrame mainFrame = new JFrame("First swing program ");
        mainFrame.setTitle("Change title");
        mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        mainFrame.setVisible(true);
    }
}

class ButtonMainFrame extends JFrame {
    public ButtonMainFrame() {

    }
}

        /*
        //JTextField - Для одной строки, расширяется только по ширине
        //gui
        JFrame mainFrame = new JFrame("First swing program");

        //mainFrame.setTitle("Change title"); - изменение заголовка окна
        mainFrame.setBounds(100, 100, 400, 300);
        mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); //остановка выполнения
        mainFrame.setMinimumSize(new Dimension(400, 300));

       // JScrollPane scroll = new JScrollPane( ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER, ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);

        JTextArea textArea = new JTextArea();
        textArea.setBackground(Color.GRAY);
        textArea.setForeground(Color.WHITE);

        //scroll.add(textArea);
        textArea.setFont(new Font("Arial", Font.BOLD, 18));//задает шрифт по умолчанию
        //JScrollPane scrollPane = new JScrollPane(textArea); // панель

        JButton clearBtn = new JButton("Clear");
        JButton incFontSizeBtn = new JButton("+");
        JButton decFontSizeBtn = new JButton("-");

      //  mainFrame.add(scroll, BorderLayout.CENTER);
        mainFrame.add(textArea, BorderLayout.CENTER);
        mainFrame.add(clearBtn, BorderLayout.NORTH);
        mainFrame.add(incFontSizeBtn, BorderLayout.EAST);
        mainFrame.add(decFontSizeBtn, BorderLayout.WEST);



        clearBtn.addActionListener(new ClearButtonListener(textArea));
        incFontSizeBtn.addActionListener(new IncFontSizeListener(textArea));
        decFontSizeBtn.addActionListener(new DecFontSizeListener(textArea));
//        textArea.setFont(new Font("Arial", Font.BOLD, 18));
//        //textArea.getFont().getSize()-1;
        textArea.setLineWrap(true);
        textArea.setWrapStyleWord(true); //автоматический перенос слов

        mainFrame.setVisible(true);
*/



//        JButton northButton = new JButton("NORTH");
//        JButton southButton = new JButton("SOUTH");
//        JButton westButton = new JButton("WEST");
//        JButton eastButton = new JButton("EAST");
//        JButton centerButton = new JButton("CENTER");
//
//        mainFrame.add(northButton, BorderLayout.NORTH);
//        mainFrame.add(southButton, BorderLayout.SOUTH);
//        mainFrame.add(westButton, BorderLayout.WEST);
//        mainFrame.add(eastButton, BorderLayout.EAST);
//        mainFrame.add(centerButton, BorderLayout.CENTER);

//        btn1.setBounds(20,20,40,20);
//
//        mainFrame.add(btn1);





/*
BorderLayout
1 - EAST/WEST - заполнятся по высоте на сколько это возможно
                ширина выбирается минимально необходимая для отображения
                сожержимого (текста)
2 - SOUTH/NORTH - заполняются всегда по всей ширине окна
                  высота выбирается по содержимому
3- CENTER - заполняет все оставшееся свободно место

 */
/**
 *===================ДЗ====================
 * 1) Доделать изсенение размера шрифта
 * 2) Научиться менять цвет текста, фона (не только в JTextArea, но и в JButton, JFrame)
 * 3) Добавить перенос строк в JTextArea
 * 4) скролл JScrollPane
 *
 * КРЕСТИКИ-НОЛИКИ:
 * 1) Создать игровое поле 3х4
 *
 * ТЕОРИЯ:
 * 1) BorderLayout
 * 2) GridLayout
 * 3) JPanel
 * 4) ознакомится с другими менеджерами компоновки
 **/
