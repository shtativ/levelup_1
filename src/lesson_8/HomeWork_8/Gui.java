package lesson_8.HomeWork_8;

import lesson_8.HomeWork_8.listeners.StepListener;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Gui {
    public void bild() {
        JFrame mainFrame = new JFrame("TicTacToe");
        mainFrame.setBounds(100, 100, 400, 400);
        mainFrame.setMinimumSize(new Dimension(400, 300));
        mainFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        //mainFrame.setLayout(new GridLayout(3, 3));
        //Анонимные классы
        //JButton quitBtn = new JButton("Quit");

        //FlawLayout по умолчанию
        JPanel battlefield = new JPanel(new GridLayout(3,3));
        JPanel menu = new JPanel(new GridLayout(1, 3));



        JButton quitBtn = new JButton("Quit");
        JButton newGameBtn = new JButton("New game");

        quitBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }


        });
        //массив кнопок-полей
        JButton[] btns = new JButton[9];
        StepListener listener = new StepListener();
        for (int i = 0; i < btns.length ; i++) {
            btns[i] = new JButton("");
            btns[i].addActionListener(listener); //обработчик событий
          //  mainFrame.add(btns[i]); - не нужно
            battlefield.add(btns[i]);
        }

        menu.add(newGameBtn, BorderLayout.WEST);
        menu.add(quitBtn, BorderLayout.EAST);

        mainFrame.add(battlefield, BorderLayout.CENTER);
        mainFrame.add(menu, BorderLayout.SOUTH);
        mainFrame.setVisible(true);
    }

}
