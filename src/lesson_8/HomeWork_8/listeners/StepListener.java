package lesson_8.HomeWork_8.listeners;

import javax.swing.*;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class StepListener implements ActionListener {
    private JButton button;
    private boolean isX = true;

    public StepListener(JButton button) {
        this.button = button;
    }

    public StepListener() {
    }

    public void actionPerformed(ActionEvent e) {
        //нажатие на кнопку
        JButton button = (JButton) e.getSource();
       // if (button.getText().equals("")) {  //способ 1: проверка двойного нажатия
            if (isX) {
                button.setText("X");
            } else {
                button.setText("O");
            }
            isX = !isX; //инвертируем isX
        button.setEnabled(false);//способ 2: блокирование кнопки после нажатия
        }
    }
//}

//8 на х и 8 ходов - победителей для 0, с помощью if-else из отдельного класса
