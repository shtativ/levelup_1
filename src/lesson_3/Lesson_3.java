package lesson_3;

import java.util.Scanner;

/**
 * Created by shtativ on 25.09.15.
 */
public class Lesson_3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        //один scanner на весь код, сам не уничтожается после запуска
        System.out.println("Введите a: ");
        double a = scanner.nextDouble(); //блокирующий метод
        System.out.println("Введите b: ");
        double b = scanner.nextDouble();
        System.out.println("Введите c: ");
        double c = scanner.nextDouble();
        double x1, x2;
        double D;
        D = (Math.pow(b, 2)) - (4 * a * c);
        System.out.println("D = " + D);
        //вычссление значенияS переменной x, в зависимости от полученного дискриминанта
        if (D > 0) {
            x1 = (-b + Math.sqrt(D)) / (2 * a);
            x2 = (-b - Math.sqrt(D)) / (2 * a);
            System.out.println("Так как D > 0, то уравнение имеет два корня: \n x1 = " + x1 + "\n x2 = " + x2);
        }
        else {
            if (D == 0){
                x1 = (-b + Math.sqrt(D)) / (2 * a);
                x2 = x1;
                System.out.println("Так как D = 0, то уравнение имеет два равных корня: /\nx1 = " + x1 + "\nx2 = " + x2);
            }
            else
                //D < 0
                System.out.println("Так как D < 0, то корни уравнения не вещественные");
            //добавить комплексные числа

        }
    }
}
